#!/usr/bin/ruby
# Simple ED-ID script; requires ruby-ldap built against OpenLDAP (with SASL)
# and OpenSSL.  Add error checking at your own discretion.

require 'ldap'

HOST, PORT = 'id.directory.vt.edu', 389
BASEDN = 'ou=People,dc=vt,dc=edu'
FILTER = 'uupid=UUPID'
ATTRIBUTE = 'eduPersonAffiliation'
VALUE = 'VT-ACTIVE-MEMBER'

dn =  nil

ldap = LDAP::SSLConn.new(HOST, PORT, true)
ldap.sasl_quiet = true
ldap.sasl_bind('', 'EXTERNAL')

# search for affiliations
ldap.search(BASEDN, LDAP::LDAP_SCOPE_SUBTREE, FILTER,
             ['eduPersonAffiliation']) do |entry|
    dn = entry.get_dn
    puts dn
    puts entry.get_values(ATTRIBUTE)
    end

# determine if we have a specific affiliation
if ldap.compare(dn, ATTRIBUTE, VALUE) == true
    puts dn + ':  ' + ATTRIBUTE + ' == ' + VALUE
else
    puts dn + ':  ' + ATTRIBUTE + ' != ' + VALUE
end

ldap.unbind
