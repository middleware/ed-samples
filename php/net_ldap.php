<?php

require_once 'Net/LDAP.php';

$host = 'directory.vt.edu';
$port = 389;
$base = 'ou=people,dc=vt,dc=edu';
$uupid = 'UUPID';
$pass = 'PASS';

$config = array (
    'basedn'    => $base,
    'host'      => $host,
    'port'      => $port,
    'starttls'  => true);

$ldap = Net_LDAP::connect($config);

if(Net_LDAP::isError($ldap))
{
    die('Could not connect to LDAP server: '.$ldap->getMessage());
}

$filter = "uupid=$uupid";

$search = $ldap->search($base, $filter);

if(Net_LDAP::isError($search))
{
    die('Could not fetch entry: '.$search->getMessage());
}

$entry = $search->entries();

if($entry)
{
    $dn = $entry[0]->dn();

    $result = $ldap->bind($dn,  $pass);
    if(Net_LDAP::isError($result))
    {
        die($result->getMessage()."\n");
    }
    else
    {
        echo "Authenticate success\n";
    }
}
else
{
    echo "No entries found for uupid = $uupid\n";
}

$ldap->done();
?>
