<?php

$host = 'ldap://id.directory.vt.edu';
$baseDn = 'ou=People,dc=vt,dc=edu';
$userfield = 'uupid';
$pid = "UUPID";
$compareAttr = 'eduPersonAffiliation';
$compareValue = 'VT-ACTIVE-MEMBER';

$ldap = ldap_connect($host);
ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_start_tls($ldap);
ldap_sasl_bind($ldap, null, null, "EXTERNAL");

if (isset($ldap) && $ldap != '') {
    /* search for pid dn */
    $result = @ldap_search($ldap, $baseDn, $userfield.'='.$pid);
    if ($result != 0) {
        $entries = ldap_get_entries($ldap, $result);
        for ($i = 0; $i < $entries["count"]; $i++) {
            $dn = $entries[$i]["dn"];
            print "dn: $dn\n";
            /* print out attributes and values */
            for ($j = 0; $j < $entries[$i]["count"]; $j++) {
                $attrib = $entries[$i][$j];
                for ($k = 0; $k < $entries[$i][$attrib]["count"]; $k++) {
                    print $attrib.": ".$entries[$i][$attrib][$k]."\n";
                }
            }
            /* check for affiliation */
            print("\nDoes user ".$compareAttr."=".$compareValue.": ");
            $cmp = ldap_compare($ldap, $dn, $compareAttr, $compareValue);
            if($cmp == true) {
                print("YES\n");
            } else {
                print("NO\n");
            }
        }
        
        ldap_free_result($result);
    } else {
        print('Error occured searching the LDAP');
    }
    ldap_close($ldap);
} else {
    print('Could not connect to LDAP at '.$host);
}
?>
