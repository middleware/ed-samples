<?php

$host = 'ldap://authn.directory.vt.edu';
$baseDn = 'ou=People,dc=vt,dc=edu';
$userfield = 'uupid';
$pid = 'UUPID';
$credential = 'PASSWORD';
$attr = 'eduPersonAffiliation';
$value = 'VT-ACTIVE-MEMBER';
$groupAttr = 'groupMembership';
$group = 'uugid=department.staff,ou=Groups,dc=vt,dc=edu';

/*ldap will bind anonymously, make sure we have some credentials*/
if (isset($pid) && $pid != '' && isset($credential)) {
    $ldap = ldap_connect($host);
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    if (!@ldap_start_tls($ldap)) {
        print('Could not start TLS');
    } else if (isset($ldap) && $ldap != '') {
        /* search for pid dn */
        $result = @ldap_search($ldap, $baseDn, $userfield.'='.$pid, array('1.1'));
        if ($result != 0) {
            $entries = ldap_get_entries($ldap, $result);
            $principal = $entries[0]['dn'];
            if (isset($principal)) {

                /* bind as this user */
                if (@ldap_bind($ldap, $principal, $credential)) {
                    print("Authenticate success\n");
                    /* determine if the user has the $attr */
                    if(@ldap_compare($ldap, $principal, $attr, $value) === true) {
                       print("$principal has $attr = $value\n");
                    } else {
                       print("$principal does not have $attr = $value\n");
                    }
                    /* determine if the user is in the $group */
                    if(@ldap_compare($ldap, $principal, $groupAttr, $group) === true) {
                       print("$principal is a member of $group\n");
                    } else {
                       print("$principal is not a member of $group\n");
                    }
                    /* Do a base search on the dn to view all
                       eduPersonAffilation(s). */
                    $r = ldap_read($ldap, $principal, $userfield.'='.$pid);
                    $e  = ldap_first_entry($ldap, $r);
                    $attrs = ldap_get_attributes($ldap, $e);
                    print("$attr: \n");
                    for($i = 0; $i < $attrs[$attr]['count'];
                        $i++)
                    {
                        print("\t".$attrs[$attr][$i]."\n");
                    }
                    ldap_free_result($r);
                } else {
                    print('Authenticate failure');
                }
            } else {
                print('User not found in LDAP');
            }
            ldap_free_result($result);
        } else {
            print('Error occured searching the LDAP');
        }
        ldap_close($ldap);
    } else {
        print('Could not connect to LDAP at '.$host);
    }
}
?>
