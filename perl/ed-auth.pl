#!/usr/bin/perl

use Net::LDAP;
use strict;

my $server = "authn.directory.vt.edu";
my $port = 389;
my $dn = "";
my $pass = "PASSWORD";
my $cafile = "/PATH/TO/ED_AUTH/CA_file.pem";
my $base = "ou=People,dc=vt,dc=edu";
my $filter = "(uupid=UUPID)";

my $ldap = Net::LDAP->new($server, port => $port, version => 3) or die $@;

my $mesg = $ldap->start_tls(verify => 'require',
                            cafile => $cafile,
                            ciphers    => 'AES256-SHA256', sslversion => 'tlsv1_2');
$mesg->code && die $mesg->error;

$mesg = $ldap->search(base => $base, filter => $filter);
$mesg->code && die $mesg->error;

my $entry = $mesg->entry(0);
if($entry)
{
    $dn = $entry->dn;
}

$mesg = $ldap->bind( dn => $dn, password => $pass);
$mesg->code && die $mesg->error;

$mesg = $ldap->search(base => $base, filter => $filter,
                      attrs => ['eduPersonPrimaryAffiliation',
                                'eduPersonAffiliation']);
$mesg->code && die $mesg->error;

$entry = $mesg->entry(0);
if($entry)
{
    print $entry->get_value('edupersonprimaryaffiliation')."\n";
    my $ref = $entry->get_value('edupersonaffiliation', asref => 1);
    foreach(@{$ref}){ print $_."\n"; }
}

# print all attributes for the entry
#foreach $entry ($mesg->all_entries) { $entry->dump; }

$ldap->unbind;
