#!/usr/bin/perl

use Net::LDAP;
use Authen::SASL;
use strict;

my $server = "id.directory.vt.edu";
my $port = 389;
my $dn = "";
my $cafile = "/PATH/TO/ED_ID/CA_file.pem";
my $clientcert = "/PATH/TO/ED_ID/clientcert.pem";
my $clientkey = "/PATH/TO/ED_ID/clientkey.pem";
my $base = "ou=people,dc=vt,dc=edu";
my $filter = "(uupid=UUPID)";
my $attr = "eduPersonAffiliation";
my $value = "VT-ACTIVE-MEMBER";
my $sasl = Authen::SASL->new(mechanism => 'EXTERNAL',
                             callback  => { pass => '', user => ''});

my $ldap = Net::LDAP->new($server, port => $port, version => 3) or die $@;

my $mesg = $ldap->start_tls(verify     => 'require',
                            cafile     => $cafile,
                            ciphers    => 'AES256-SHA256', sslversion => 'tlsv1_2',
                            clientcert => $clientcert,
                            clientkey  => $clientkey);
$mesg->code && die $mesg->error;

$mesg = $ldap->bind(dn   => '',
                    sasl => $sasl);
$mesg->code && die $mesg->error;

$mesg = $ldap->search(base => $base, filter => $filter);
$mesg->code && die $mesg->error;

# print specific attribute
#$entry = $mesg->entry(0);
#if($entry)
#{
#    print $entry->get_value('edupersonprimaryaffiliation')."\n";
#    my $ref = $entry->get_value('edupersonaffiliation', asref => 1);
#    foreach(@{$ref}){ print $_."\n"; }
#}

# print all attributes for the entry
foreach my $entry ($mesg->all_entries) { $entry->dump; }

my $entry = $mesg->entry(0);
$mesg = $ldap->compare($entry,
                       attr => $attr,
                       value => $value);
if($mesg->code == 6) # error code 6 is LDAP_COMPARE_TRUE
{
    print $entry->dn.": $attr == $value\n";
}
else
{
    print $entry->dn.": $attr != $value\n";
}

$ldap->unbind;