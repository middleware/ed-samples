/**
 * edid.c
 * This code is an example of how to connect to ED-ID, do a
 * SASL EXTERNAL bind with a client certificate, search for a user,
 * print out the user's attributes, and then determine if the user
 * has the proper affiliation specified.  This is but the tip of the
 * iceberg for authorization that can be done with ED-ID.
 *
 * Notes: * You must have a client certificate that has been issued
 *			by the VT Middleware CA to connect to ED-ID.
 *		  * You must have a service entry that corresponds to your
 *		    client certificate to be able to view entries
 *        * The VT Root CA certificate must be installed into the Local Machine
 *          certificate store prior to running this sample.
 *          Root certificate is available at
 *          https://4help.vt.edu/sp?id=kb_article&sysparm_article=KB0011371
 *        * Your client certificate must be installed into the Local Machine
 *          certificate store prior to running this sample.
 *        * You must link this against wldap32.lib
 */

#include <windows.h>
#include <ntldap.h>
#include <winldap.h>
#include <stdio.h>
#include <winber.h>

/**
 * Do a SASL EXTERNAL bind, search for the user with the supplied UUPID,
 * print all attributes for the person, determine if the person has the
 * specified affiliation.
 */
int main(int argc, char* argv[])
{
    LDAP* ld = NULL;
    INT retVal = 0;
	PCHAR pHost = "id.directory.vt.edu";
	int port = 636;
	char* base = "ou=people,dc=vt,dc=edu";
	char* filter = "(uupid=UUPID)";
	LDAPMessage *result, *entry;
	char *dn;
	char *cmpAttr = "virginiaTechAffiliation";
	char *cmpVal  = "VT-ACTIVE-MEMBER";
	struct berval cred;
	struct berval *servercredp;
    ULONG version = LDAP_VERSION3;
	char *attribute;
	BerElement *ber;
	char **values;
	int i;

	cred.bv_val = "";
	cred.bv_len = strlen(cred.bv_val)*sizeof(char);

	printf("\nConnecting to %s:%d ...\n", pHost, port);

    // Create an LDAP session.
	ld = ldap_sslinit(pHost, port, 1);
    if (ld == NULL)
    {
        printf( "ldap_sslinit failed with 0x%x.\n",GetLastError());
        return -1;
    }

    // Specify version 3; the default is version 2.
    printf("Setting Protocol version to 3.\n");
    retVal = ldap_set_option(ld,
                           LDAP_OPT_PROTOCOL_VERSION,
                           (void*)&version);

	retVal = ldap_set_option(ld,LDAP_OPT_SSL,LDAP_OPT_ON);

    // Connect to the server.
    retVal = ldap_connect(ld, NULL);

	if(retVal == LDAP_SUCCESS)
        printf("ldap_connect succeeded \n");
    else
    {
        printf("ldap_connect failed with 0x%x.\n",retVal);
		return 1;
    }

	// Perform a SASL EXTERNAL bind.  The CN of your client certificate
	// will be your service's UUSID
	retVal = ldap_sasl_bind_s(ld, "", "EXTERNAL" , &cred, NULL, NULL, &servercredp);

	if(retVal != LDAP_SUCCESS)
		printf("ldap_sasl_bind_s failed with 0x%x\n", retVal);
	else
		printf("ldap_sasl_bind_s succeeded\n");

	// Search for a person by UUPID
	retVal = ldap_search_s(ld, base, LDAP_SCOPE_SUBTREE, filter, NULL, NULL, &result);

	if(retVal != LDAP_SUCCESS)
		printf("ldap_search_s failed with 0x%x.\n",retVal);

	// Get the first entry and its DN
	entry = ldap_first_entry(ld, result);
	dn = ldap_get_dn(ld, entry);

	// Print out all viewable attributes for the person
	if(entry != NULL)
	{
		for(attribute = ldap_first_attribute(ld, entry, &ber);
			attribute != NULL;
			attribute = ldap_next_attribute(ld, entry, ber))
		{
			if((values = ldap_get_values(ld, entry, attribute)) != NULL)
			{
				for(i = 0; values[i] != NULL; i++)
				{
					printf("%s: %s\n", attribute, values[i]);
				}
				ldap_value_free(values);
			}
			ldap_memfree(attribute);
		}
		
		if(ber != NULL)
		{
			ber_free(ber, 0);
		}
	}

	ldap_msgfree(result);

	// Determine if the person has the specified affiliation
	retVal = ldap_compare_s(ld, dn, cmpAttr, cmpVal);

	if(retVal != LDAP_COMPARE_TRUE)
		printf("ldap_compare_s failed with 0x%x.\n",retVal);
	else
		printf("\n%s == %s", cmpAttr, cmpVal);

	ldap_memfree(dn);
    ldap_unbind_s(ld);
    return 0;
}
