/**
 * edauth.c
 * This code is an example of how to connect to ED-Auth,
 * search for an entry by a person's UUPID, bind as that
 * UUPID, and then determine if that person is an active
 * member at VT with the winldap library.
 * This illustrates the basic authentication/
 * authorization ED-Auth is to be used for.
 *
 * Notes:  * The VT Root CA certificate must be installed into the Local Machine
 *           certificate store prior to executing following code.
 *           Root certificate is available at 
 *           https://4help.vt.edu/sp?id=kb_article&sysparm_article=KB0011371
 *         * You must link this against wldap32.lib
 */

#include <windows.h>
#include <stdio.h>
#include <ntldap.h>
#include <winldap.h>

/**
 * Search for the DN with the supplied UUPID, bind as that DN with the
 * given credentials, and then determine if the entry has the
 * specified affiliation.
 */
int main(int argc, char* argv[])
{
    LDAP* ld = NULL;
    INT retVal = 0;
	PCHAR pHost = "authn.directory.vt.edu";
	int port = 636;
	char* base = "ou=People,dc=vt,dc=edu";
	char* filter = "(uupid=UUPID)";
	LDAPMessage *result, *entry;
	char *dn;
	char *pass = "PASSWORD";
	char *cmpAttr = "virginiaTechAffiliation";
	char *cmpVal  = "VT-ACTIVE-MEMBER";

    ULONG version = LDAP_VERSION3;
    LONG lv = 0;

	printf("\nConnecting to %s:%d ...\n", pHost, port);
	
    // Create an LDAP session.
	ld = ldap_sslinit(pHost, port, 1);
    if (ld == NULL)
    {
        printf( "ldap_sslinit failed with 0x%x.\n",GetLastError());
        return -1;
    }
	
    // Specify version 3; the default is version 2.
    printf("Setting Protocol version to 3.\n");
    retVal = ldap_set_option(ld,
                           LDAP_OPT_PROTOCOL_VERSION,
                           (void*)&version);
    if (retVal != LDAP_SUCCESS)
        return 1;

	retVal = ldap_set_option(ld,LDAP_OPT_SSL,LDAP_OPT_ON);

    // Connect to the server.
    retVal = ldap_connect(ld, NULL);

    if(retVal == LDAP_SUCCESS)
        printf("ldap_connect succeeded \n");
    else
    {
        printf("ldap_connect failed with 0x%x.\n",retVal);
		return 1;
    }

	// Search for the UUPID
	retVal = ldap_search_s(ld, base, LDAP_SCOPE_SUBTREE, filter, NULL, NULL, &result);

	if(retVal != LDAP_SUCCESS)
	{
		printf("ldap_search_s failed with 0x%x.\n",retVal);
		return 1;
	}

	// Get the DN
	entry = ldap_first_entry(ld, result);
	dn = ldap_get_dn(ld, entry);
	ldap_msgfree(result);
	
	// Bind with current credentials.
    printf("Binding with dn %s...\n", dn);
    retVal = ldap_bind_s(ld,dn,pass,LDAP_AUTH_SIMPLE);
    if (retVal != LDAP_SUCCESS)
        printf("Bind failed with 0x%x.\n", retVal);
	else
		printf("Bind as %s succeeded.\n", dn);

	// Determine if this person has the affiliation we want
	retVal = ldap_compare_s(ld, dn, cmpAttr, cmpVal);
	if(retVal != LDAP_COMPARE_TRUE)
		printf("ldap_compare_s failed with 0x%x.\n",retVal);
	else
		printf("%s == %s", cmpAttr, cmpVal);

	ldap_memfree(dn);
    ldap_unbind_s(ld);
    return 0;
}
