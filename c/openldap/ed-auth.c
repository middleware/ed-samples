#include <stdio.h>
#include <ldap.h>

int main(int argc, char* argv[])
{
    LDAP* ldap;
    LDAPMessage *result, *entry;
    BerElement* ber;
    char *HOST_NAME = "authn.directory.vt.edu";
    int PORT_NUMBER = 389;
    char *PASSWORD = "PASSWORD";
    char *filter = "(uupid=UUPID)";
    char  *attrs[] = {"eduPersonPrimaryAffiliation",
                     "eduPersonAffiliation", NULL};
    char *base = "ou=People,dc=vt,dc=edu";
    char *attribute = NULL;
    char **values = NULL;
    char *dn = NULL;
    char *cmpAttr = "eduPersonAffiliation";
    char *cmpVal  = "VT-ACTIVE-MEMBER";
    int i, resultCode, version;

    /* Initialize an LDAP connection. */
    if( (ldap = ldap_init(HOST_NAME, PORT_NUMBER)) == NULL)
    {
        perror("ldap_init");
        return 1;
    }

    /* Set the version number so that we may use startTLS. */
    version = LDAP_VERSION3;
    ldap_set_option(ldap, LDAP_OPT_PROTOCOL_VERSION, &version);

    if(ldap_start_tls_s(ldap, NULL, NULL) != LDAP_SUCCESS)
    {
        ldap_perror(ldap, "ldap_start_tls");
    }

    resultCode = ldap_search_ext_s(ldap, base, LDAP_SCOPE_SUBTREE,
                                   filter, NULL, 0, NULL, NULL,
                                   NULL, LDAP_NO_LIMIT, &result);
    if(resultCode != LDAP_SUCCESS)
    {
        /* another way to print errors...
        fprintf(stderr, "ldap_search_ext_s: %s\n",
                ldap_err2string(resultCode));
        */
        ldap_perror(ldap, "ldap_search_ext_s");
        ldap_unbind_ext_s(ldap, NULL, NULL);
        return 1;
    }

    entry = ldap_first_entry(ldap, result);
    if(entry != NULL)
    {
        dn = ldap_get_dn(ldap, entry);
        ldap_msgfree(result);
    }
    else
    {
        printf("search on filter: %s returned no entries\n", filter);
        ldap_msgfree(result);
        ldap_unbind_ext_s(ldap, NULL, NULL);
        return 1;
    }

    /* Bind as a user. If PASSWORD is NULL, resultCode will be LDAP_UNWILLING_TO_PERFORM.
       Always make sure password is not NULL. */
    resultCode = ldap_simple_bind_s(ldap, dn, PASSWORD);
    if(resultCode != LDAP_SUCCESS)
    {
        ldap_perror(ldap, "ldap_simple_bind_s");
        ldap_memfree(dn);
        return 1;
    }

    /* Search for the user. */
    resultCode = ldap_search_ext_s(ldap, dn, LDAP_SCOPE_BASE,
                                   filter, attrs, 0, NULL, NULL,
                                   NULL, LDAP_NO_LIMIT, &result);
    if( resultCode != LDAP_SUCCESS)
    {
        ldap_perror(ldap, "ldap_search_ext_s");
        ldap_memfree(dn);
        return 1;
    }

    /* Since we are doing a base search, there should be only one
       matching entry */
    entry = ldap_first_entry(ldap, result);
    if(entry != NULL)
    {
        printf("\ndn: %s\n", dn);
        /* Iterate through each attribute in the entry. */
       for( attribute = ldap_first_attribute(ldap, entry, &ber);
            attribute != NULL;
            attribute = ldap_next_attribute(ldap, entry, ber))
       {
           /*For each attribute, print the name and values.*/
           values = ldap_get_values(ldap, entry, attribute);
           if( values != NULL)
           {
               for(i = 0; values[i] != NULL; i++)
               {
                   printf("%s: %s\n", attribute, values[i]);
               }
               ldap_value_free(values);
           }
           ldap_memfree(attribute);
       }
       if(ber != NULL)
       {
           ber_free(ber, 0);
       }
    }

    /* see if user has a specific affiliation */
    resultCode = ldap_compare_s(ldap, dn, cmpAttr, cmpVal);
    if(resultCode == LDAP_COMPARE_TRUE)
        printf("ldap_compare_s: %s has %s=%s\n", dn, cmpAttr, cmpVal);
    else
        printf("ldap_compare_s: %s does not have %s=%s\n", dn, cmpAttr, cmpVal);
    ldap_msgfree(result);
    ldap_memfree(dn);
    ldap_unbind_ext_s(ldap, NULL, NULL);
    return 0;
}
