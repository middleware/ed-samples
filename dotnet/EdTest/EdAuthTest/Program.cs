﻿using System;
using System.Net;
using System.DirectoryServices.Protocols;
using System.Security.Cryptography.X509Certificates;

using EdCommon;

namespace EdAuthTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("USAGE: EdAuthTest uupid [dev|pprd|prod]");
                return;
            }
            string uupid = args[0];
            string password = GetPassword(string.Format("Password for {0}:", uupid));
            string ldapHost = EdConstants.ED_AUTH;
            int ldapPort = 389;
            if (args.Length > 1)
            {
                switch (args[1])
                {
                    case "dev":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_DEV;
                        break;
                    case "pprd":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_PPRD;
                        break;
                }
            }
            string ldapFilter = "uupid=" + uupid;

            Console.WriteLine(string.Format("Attempting to authenticate to {0} as {1}", ldapHost, uupid));

            // Create connection and attempt to bind and search
            LdapConnection conn = null;
            try
            {
                conn = new LdapConnection(
                  new LdapDirectoryIdentifier(ldapHost, ldapPort),
                  null,
                  AuthType.Basic);
                // VT Enterprise Directory requires LDAPv3
                conn.SessionOptions.ProtocolVersion = 3;

                // Must use custom hostname verification strategy due to DNS aliases
                conn.SessionOptions.VerifyServerCertificate += new VerifyServerCertificateCallback(
                    new EdCertificateVerifier(ldapHost).VerifyCertificate);

                // A QueryClientCertificateCallback is required based on our testing.
                // Investigation suggests that this handler is required when the SSL handshake
                // contains a client certificate request directive.
                conn.SessionOptions.QueryClientCertificate =
                delegate(LdapConnection c, byte[][] trustedCAs)
                {
                    return null;
                };

                // Must conduct initial search over TLS connection to overcome suppressed PIDs
                conn.SessionOptions.StartTransportLayerSecurity(null);

                // Bind anonymously for initial search
                conn.Bind();

                // Search for the DN of the user with given uupid
                SearchResponse response = (SearchResponse)conn.SendRequest(
                  new SearchRequest(EdConstants.SEARCH_BASE, ldapFilter, SearchScope.Subtree, "dn"));

                if (response.Entries.Count == 0)
                {
                    throw new ArgumentException("Cannot find DN for uupid=" + uupid);
                }
                string dn = response.Entries[0].DistinguishedName;
                Console.WriteLine("Found user DN " + dn);

                // Rebind as authenticated user
                conn.Bind(new NetworkCredential(dn, password));

                // 4th parameter, attributeList, is omitted to indicate all available attributes
                response = (SearchResponse)conn.SendRequest(
                    new SearchRequest(dn, ldapFilter, SearchScope.Base));

                // Print attributes of entry
                SearchResultEntry entry = response.Entries[0];
                foreach (String name in entry.Attributes.AttributeNames)
                {
                    Console.Write("      " + name + "=");
                    int n = 0;
                    foreach (object value in entry.Attributes[name].GetValues(typeof(string)))
                    {
                        if (n++ > 0)
                        {
                            Console.Write(',');
                        }
                        Console.Write(value);
                    }
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Application error: \n" + e);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }
        }

        /// <summary>
        /// Lightly adapted from http://msdn.microsoft.com/en-us/library/ms733131.aspx.
        /// </summary>
        static string GetPassword(string prompt)
        {
            Console.WriteLine(prompt);
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    password += info.KeyChar;
                    info = Console.ReadKey(true);
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        password = password.Substring
                        (0, password.Length - 1);
                    }
                    info = Console.ReadKey(true);
                }
            }
            for (int i = 0; i < password.Length; i++)
                Console.Write("*");
            Console.WriteLine();
            return password;
        }
    }
}
