﻿using System;
using System.DirectoryServices.Protocols;

namespace EdCommon
{
    public static class EdConstants
    {
        public const string ED_AUTH = "authn.directory.vt.edu";
        public const string ED_ID = "id.directory.vt.edu";
        public const string ED_LITE = "directory.vt.edu";
        public const string ED = "ed.middleware.vt.edu";
        public const string ED_DEV = "ed-dev.middleware.vt.edu";
        public const string ED_PPRD = "ed-pprd.middleware.vt.edu";
        public const string SEARCH_BASE = "ou=People,dc=vt,dc=edu";
        public const int ALT_PORT = 10389;
    }
}
