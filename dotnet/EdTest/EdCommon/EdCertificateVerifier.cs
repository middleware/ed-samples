﻿using System;
using System.Diagnostics;
using System.DirectoryServices.Protocols;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace EdCommon
{
    public class EdCertificateVerifier
    {
        public const string SUBJECT_ALT_NAME_OID = "2.5.29.17";
        private static readonly Regex CN_REGEX = new Regex(@"CN\s*=\s*([A-Za-z0-9._-]+)");

        public EdCertificateVerifier(string hostName)
        {
            if (hostName == null)
            {
                throw new ArgumentNullException("hostName");
            }
            HostName = hostName;
        }

        public string HostName
        {
            get;
            protected set;
        }

        public bool VerifyCertificate(LdapConnection connection, X509Certificate certificate)
        {
            Trace.WriteLine("Verifying certificate for host " + connection.SessionOptions.HostName);
            X509Certificate2 extendedCert = new X509Certificate2(certificate);
            
            // Perform standard X.509 path validation
            bool valid = extendedCert.Verify();

            // Custom hostname verification
            if (valid)
            {
                valid = CommonNameMatches(extendedCert) || SubjectAltNameMatches(extendedCert);
            }
            return valid;
        }

        private bool CommonNameMatches(X509Certificate2 cert)
        {
            Match m = CN_REGEX.Match(cert.Subject);
            string cn = null;
            if (m.Success && m.Groups.Count > 0)
            {
                cn = m.Groups[1].Value;
            }
            return string.Compare(cn, HostName, true) == 0;
        }

        private bool SubjectAltNameMatches(X509Certificate2 cert)
        {
            X509Extension subjectAltNames = cert.Extensions[SUBJECT_ALT_NAME_OID];
            if (subjectAltNames != null)
            {
                foreach (string altName in subjectAltNames.Format(true).Split('\n'))
                {
                    Trace.WriteLine("Evaluating subject alt name " + altName);
                    if (altName.Trim().EndsWith(HostName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
