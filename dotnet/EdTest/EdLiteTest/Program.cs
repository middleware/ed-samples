﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.DirectoryServices.Protocols;

using EdCommon;

namespace EdLiteTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("USAGE: EdLiteTest query attributeList [dev|pprd|prod]");
                return;
            }
            string ldapQuery = args[0];
            string[] attributes = args[1].Split(',');
            string ldapHost = EdConstants.ED_LITE;
            int ldapPort = 389;
            if (args.Length > 2)
            {
                switch (args[2])
                {
                    case "dev":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_DEV;
                        break;
                    case "pprd":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_PPRD;
                        break;
                }
            }

            Console.WriteLine(
                string.Format("Querying {0} with query {1} for attributes {2}",
                    ldapHost, ldapQuery, String.Join(",", attributes)));

            // Create connection and perform anonymous query
            LdapConnection conn = null;
            try
            {
                conn = new LdapConnection(
                  new LdapDirectoryIdentifier(ldapHost, ldapPort),
                  null,
                  AuthType.Anonymous);
                // VT Enterprise Directory requires LDAPv3
                conn.SessionOptions.ProtocolVersion = 3;
                conn.Bind();

                // Query for authId (i.e. PID) and name parts
                // Suppressed entries/attributes not available in ED-Lite
                SearchResponse response = (SearchResponse)conn.SendRequest(
                  new SearchRequest(EdConstants.SEARCH_BASE, ldapQuery, SearchScope.Subtree, attributes));

                // Print attributes of result entries
                Console.WriteLine();
                Console.WriteLine(response.Entries.Count + " entries found:");
                foreach (SearchResultEntry entry in response.Entries)
                {
                    Console.WriteLine("   " + entry.DistinguishedName);
                    foreach (String name in entry.Attributes.AttributeNames)
                    {
                        Console.Write("      " + name + "=");
                        int n = 0;
                        foreach (object value in entry.Attributes[name].GetValues(typeof(string)))
                        {
                            if (n++ > 0)
                            {
                                Console.Write(',');
                            }
                            Console.Write(value);
                        }
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Application error: \n" + e);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }
        }
    }
}
