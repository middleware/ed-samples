﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.DirectoryServices.Protocols;

using EdCommon;

namespace EdIdTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("USAGE: EdIdTest uusid query [dev|pprd|prod]");
                return;
            }
            // The following should be the uusid of your ED-ID service
            string certCN = args[0];
            string ldapQuery = args[1];
            string ldapHost = EdConstants.ED_ID;
            int ldapPort = 389;
            if (args.Length > 2)
            {
                switch (args[2])
                {
                    case "dev":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_DEV;
                        break;
                    case "pprd":
                        ldapPort = EdConstants.ALT_PORT;
                        ldapHost = EdConstants.ED_PPRD;
                        break;
                }
            }

            Console.WriteLine(string.Format("Querying {0} as service {1} for {2}", ldapHost, certCN, ldapQuery));

            // Create connection and attempt to bind and search
            LdapConnection conn = null;
            try
            {
                conn = new LdapConnection(
                  new LdapDirectoryIdentifier(ldapHost, ldapPort),
                  null,
                  AuthType.External);
                // VT Enterprise Directory requires LDAPv3
                conn.SessionOptions.ProtocolVersion = 3;

                // Must use custom hostname verification strategy due to DNS aliases
                conn.SessionOptions.VerifyServerCertificate = new VerifyServerCertificateCallback(
                    new EdCertificateVerifier(ldapHost).VerifyCertificate);

                // Look up client cert in Local Machine store by subject CN
                conn.SessionOptions.QueryClientCertificate =
                  delegate(LdapConnection c, byte[][] trustedCAs)
                  {
                      X509Store lmStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                      try
                      {
                          lmStore.Open(OpenFlags.ReadOnly);
                          // Uncomment the following lines to help diagnose cert problems
                          //Console.WriteLine();
                          //Console.WriteLine("Available certificates in Local Machine store:");
                          //foreach (X509Certificate cert in lmStore.Certificates)
                          //{
                          //    Console.WriteLine("   " + cert.Subject);
                          //}
                          //Console.WriteLine("Querying Local Machine store for valid cert with subject " + certCN);
                          X509Certificate2Collection clientCerts = lmStore.Certificates.Find(
                            X509FindType.FindBySubjectName, certCN, true);
                          if (clientCerts.Count == 0)
                          {
                              throw new ArgumentException("Cannot find valid certificate with subject " + certCN);
                          }
                          return clientCerts[0];
                      }
                      finally
                      {
                          lmStore.Close();
                      }
                  };
                conn.SessionOptions.StartTransportLayerSecurity(null);
                conn.Bind();

                // 4th parameter, attributeList, is omitted to indicate all available attributes
                SearchResponse response = (SearchResponse)conn.SendRequest(
                  new SearchRequest(EdConstants.SEARCH_BASE, ldapQuery, SearchScope.Subtree));

                // Stopping TLS is demonstrated for completeness.
                // Ideally ED-ID connections are pooled and the TLS session is maintained
                // for the life of the connection.
                conn.SessionOptions.StopTransportLayerSecurity();

                // Print attributes of result entries
                Console.WriteLine();
                Console.WriteLine(response.Entries.Count + " entries found:");
                foreach (SearchResultEntry entry in response.Entries)
                {
                    Console.WriteLine("   " + entry.DistinguishedName);
                    foreach (String name in entry.Attributes.AttributeNames)
                    {
                        Console.Write("      " + name + "=");
                        int n = 0;
                        foreach (object value in entry.Attributes[name].GetValues(typeof(string)))
                        {
                            if (n++ > 0)
                            {
                                Console.Write(',');
                            }
                            Console.Write(value);
                        }
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Application error: \n" + e);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }
        }
    }
}
