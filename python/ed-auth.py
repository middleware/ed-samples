#!/usr/bin/python

import ldap

base = "ou=People,dc=vt,dc=edu"
scope = ldap.SCOPE_SUBTREE
searchFilter = "uupid=UUPID"
password = "PASSWORD"
attribute = "eduPersonAffiliation";
affiliation = "VT-ACTIVE-MEMBER"

try:
    # initialize the ldap connection
    edauth = ldap.initialize("ldap://authn.directory.vt.edu:389")
    edauth.protocol_version=ldap.VERSION3
    edauth.start_tls_s()
    results = edauth.search(base, scope, searchFilter, None)
    resultType, result_data = edauth.result(results, 0)
    if result_data:
        dn = result_data[0][0]
        edauth.simple_bind_s(dn, password)
        if edauth.compare_s(dn, attribute, affiliation) == 0:
            print dn, "does not have", attribute, "=", affiliation
        else:
            print dn, "has", attribute, "=", affiliation
    else:
        print "UUPID not found"
except ldap.LDAPError,e:
    print str(e)
