#!/usr/bin/python

import ldap,ldap.sasl

base = "ou=People,dc=vt,dc=edu"
scope = ldap.SCOPE_SUBTREE
searchFilter = "uupid=UUPID"
attribute = "eduPersonAffiliation"
value = "VT-ACTIVE-MEMBER";

try:
    edid = ldap.initialize("ldap://id.directory.vt.edu:389")
    edid.protocol_version=ldap.VERSION3
    edid.start_tls_s()
    edid.sasl_interactive_bind_s("", ldap.sasl.external())
    results = edid.search(base, scope, searchFilter, None)
    resultType, result_data = edid.result(results, 1)
    dn = result_data[0][0]
    # barf out all user data for first entry returned (not pretty)
    print dn
    print result_data[0][1]
    if edid.compare_s(dn, attribute, value) == 0:
        print dn, attribute, "!=", value
    else:
        print dn, attribute, "==", value
except ldap.LDAPError,e:
    print str(e)


