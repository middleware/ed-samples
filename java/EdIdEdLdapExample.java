import java.util.Iterator;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import edu.vt.middleware.ldap.ed.DirectoryEnv;
import edu.vt.middleware.ldap.ed.EdId;

public class EdIdEdLdapExample
{
  public static void main(String[] args) throws Exception {
    if (args.length < 1) {
      System.err.println("USAGE: EdIdEdLdapExample query");
      return;
    }
    String query = args[0];
    String[] returnAttributes = {
      "givenname",
      "surname",
      "displayName"
    };
    EdId directory = new EdId(DirectoryEnv.DEV);
    Iterator<SearchResult> it = directory.search(query, returnAttributes);
    while (it.hasNext()) {
      SearchResult result = it.next();
      NamingEnumeration attributes = result.getAttributes().getAll();
      while (attributes.hasMore()) {
        Attribute attr = (Attribute) attributes.next();
        System.out.println(attr.getID());
        NamingEnumeration values = attr.getAll();
        while (values.hasMore()) {
          System.out.println("\t" + values.next());
        }
      }     
    }
  }
}
