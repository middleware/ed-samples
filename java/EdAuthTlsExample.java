import java.io.IOException;
import java.util.Hashtable;
import java.util.ArrayList;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;

public class EdAuthTlsExample
{
  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("USAGE: EdAuthTlsExample uupid pass");
      return;
    }
    String hostName = "ldap://authn.directory.vt.edu";
    String baseDn = "ou=People,dc=vt,dc=edu";
    String pid = args[0];
    String credential = args[1];

    // Set up JNDI context for an anonymous search for uupid
    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY,
        "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.PROVIDER_URL, hostName);
    env.put("java.naming.ldap.version", "3");
    LdapContext ctx = null;
    StartTlsResponse tls = null;
    try {
      ctx = new InitialLdapContext(env, null);

      // Authentication must be performed over a secure channel
      tls = (StartTlsResponse) ctx.extendedOperation(new StartTlsRequest());
      tls.negotiate();

      // Perform anonymous lookup of user DN based on uupid attribute
      BasicAttributes attrs = new BasicAttributes("uupid", pid);
      String[] retAttrs = new String[] { "dn" };
      NamingEnumeration ne1 = ctx.search(baseDn, attrs, retAttrs);
      SearchResult pidSearchResult = (SearchResult) ne1.next();
      if (pidSearchResult == null) {
        System.out.println(pid+" not found in ED-Auth");
        return;
      }
      String userDn = pidSearchResult.getNameInNamespace();
      System.out.println("Found user DN "+userDn);

      // Authenticate the user and search for privileged attributes
      // belonging to authenticated user
      ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
      ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, userDn);
      ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, credential);
      ctx.reconnect(null);
      NamingEnumeration ne2 = ctx.search(baseDn, attrs);
      while (ne2.hasMore()) {
        SearchResult sr = (SearchResult) ne2.next();
        NamingEnumeration attributes = sr.getAttributes().getAll();
        while (attributes.hasMore()) {
          Attribute attr = (Attribute) attributes.next();
          System.out.println(attr.getID());
          NamingEnumeration values = attr.getAll();
          while (values.hasMore()) {
            System.out.println("\t" + values.next());
          }
        }
      }
    } catch (IOException e) {
      System.err.println("TLS negotiation error:");
      e.printStackTrace();
    } catch (NamingException e) {
      System.err.println("JNDI error:");
      e.printStackTrace();
    } finally {
      if (tls != null) {
        try {
          // Tear down TLS connection
          tls.close();
        } catch (IOException e) {
          System.err.println("Error tearing down TLS connection.");
        }
      }
      if (ctx != null) {
        try {
          // Close LDAP connection
          ctx.close();
        } catch (NamingException e) {
          System.err.println("Error closing JNDI context.");
        }
      }
    }
  }
}
