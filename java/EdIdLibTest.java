import java.util.Iterator;
import edu.vt.middleware.ldap.LdapUtil;
import edu.vt.middleware.ldap.ed.EdId;

/**
 * <p>
 * EdIdLibTest provides a test for the EdId class.
 * </p>
 */
public final class EdIdLibTest
{
  public void doTest()
    throws Exception
  {
    final String uupid = "UUPID";
    
    final String query = "uupid="+uupid;
    final EdId id = new EdId();
    final Iterator i = id.search(query, null);
    LdapUtil.print(i, System.out);
  }

  public static void main(final String[] args)
    throws Exception
  {
    final EdIdLibTest test = new EdIdLibTest();
    test.doTest();
  }
}
