import java.io.IOException;
import java.util.Hashtable;
import java.util.ArrayList;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;

public class EdIdTlsExample
{
  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println("USAGE: EdIdTlsExample query");
      return;
    }
    String hostName = "ldap://ed-dev.middleware.iad.vt.edu:10389";
    String baseDn = "ou=People,dc=vt,dc=edu";
    String query = args[0];
    String[] returnAttributes = {
      "givenname",
      "surname",
      "displayName"
    };

    // Initial JNDI environment setup
    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY,
        "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.PROVIDER_URL, hostName);
    env.put("java.naming.ldap.version", "3");
    LdapContext ctx = null;
    StartTlsResponse tls = null;
    try {
      ctx = new InitialLdapContext(env, null);

      // Authentication must be performed over a secure channel
      tls = (StartTlsResponse) ctx.extendedOperation(new StartTlsRequest());
      tls.negotiate();
      
      // Authenticate via SASL EXTERNAL mechanism using client X.509
      // certificate contained in JVM keystore
      ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "EXTERNAL");
      ctx.reconnect(null);

      // Perform search for privileged attributes under authenticated context
      SearchControls controls = new SearchControls();
      controls.setReturningAttributes(returnAttributes);
      controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
      System.out.println("Executing query "+query);
      NamingEnumeration ne = ctx.search(baseDn, query, controls);
      while (ne.hasMore()) {
        SearchResult sr = (SearchResult) ne.next();
        System.out.println("Result DN " + sr.getNameInNamespace());
        NamingEnumeration attributes = sr.getAttributes().getAll();
        while (attributes.hasMore()) {
          Attribute attr = (Attribute) attributes.next();
          System.out.println(attr.getID());
          NamingEnumeration values = attr.getAll();
          while (values.hasMore()) {
            System.out.println("\t" + values.next());
          }
        }
      }
    } catch (IOException e) {
      System.err.println("TLS negotiation error:");
      e.printStackTrace();
    } catch (NamingException e) {
      System.err.println("JNDI error:");
      e.printStackTrace();
    } finally {
      if (tls != null) {
        try {
          // Tear down TLS connection
          tls.close();
        } catch (IOException e) {
          System.err.println("Error tearing down TLS connection.");
        }
      }
      if (ctx != null) {
        try {
          // Close LDAP connection
          ctx.close();
        } catch (NamingException e) {
          System.err.println("Error closing JNDI context.");
        }
      }
    }
  }
}
