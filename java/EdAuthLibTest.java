import edu.vt.middleware.ldap.ed.EdAuth;

/**
 * <p>
 * EdAuthLibTest provides a test for the EdAuth class.
 * </p>
 */
public final class EdAuthLibTest
{
  public void doTest()
    throws Exception
  {
    String uupid = "UUPID";
    String credential  = "PASSWORD";
    String filter = "AUTHORIZATION FILTER";

    final EdAuth auth = new EdAuth();
    if (auth.authenticateAndAuthorize(uupid, credential, filter)) {
      System.out.println("Primary affiliation: "+
                         auth.getPrimaryAffiliation(uupid, credential));
      final String[] affil = auth.getAffiliations(uupid, credential);
      System.out.println("Affiliations: ");
      for (int i = 0; i < affil.length; i++) {
        System.out.println("  "+affil[i]);
      }
    } else {
      System.out.println("Authentication or Authorization failed");
    }
  }

  public static void main(final String[] args)
    throws Exception
  {
    final EdAuthLibTest test = new EdAuthLibTest();
    test.doTest();
  }
}
